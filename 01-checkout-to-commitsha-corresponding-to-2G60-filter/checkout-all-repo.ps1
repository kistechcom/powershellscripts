function DisplayTitle {
    param (
        $Message
    )
    Write-Host $Message -ForegroundColor Yellow
}

function DisplayMessage {
    param (
        $Message
    )
    Write-Host $Message -ForegroundColor Green
}

function DisplayError {
    param (
        $ErrorMessage
    )
    Write-Host $ErrorMessage -ForegroundColor Red
}

# if ($args.Count -ne 2)
# {
# 	DisplayError "Missing argument(s)"
# 	DisplayError "Usage: demo {path to paths.txt} {path to commitsSHA.txt}. eg. .\checkout-all-repo.ps1 ./paths.txt ./commitsSHA.txt"
# 	exit 1
# }

if ($args.Count -ne 1)
{
	DisplayError "Missing argument(s)"
	DisplayError "Usage: checkout-all-repo {path to target-commits.txt}. eg. .\checkout-all-repo.ps1 ./target-commits.txt"
	# > .\checkout-all-repo.ps1 $pwd/target-commits.txt
	exit 1
}

DisplayMessage "Current working directory: "
Write-Output $PSScriptRoot
$res = 0
$commits_list_filepath = Join-Path $PSScriptRoot $args[0]
DisplayMessage $commits_list_filepath

foreach($line in [System.IO.File]::ReadLines($commits_list_filepath))
{
    Write-Output $line
    $arr = $line -split "\s+"
    DisplayMessage "Expect dir:$($arr[0])"
    Set-Location -Path $arr[0]
    DisplayMessage "Expect Commit:$($arr[1])"
    DisplayMessage "Current dir:$(Get-Location)"

    DisplayMessage "Checkout$($arr[1])"
    git checkout $arr[1]

    $comm = git rev-parse --verify HEAD
    DisplayMessage "Current Commit:$comm."
    if ($comm.Trim() -ne $arr[1].Trim()) {
        DisplayError "Commit SHA is not the same for $arr[0]"
        $res = 1
    }
}

if ($res -eq 1)
{
    DisplayError "Not All Commits Matched!"
} else {
    DisplayTitle "Successful. All Commit Are Matched!"
}

Set-Location -Path $PSScriptRoot