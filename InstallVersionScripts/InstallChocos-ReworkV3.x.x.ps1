# Set choco sources
choco source add -n kissoftwaredevrework -s="https://nexus.dev.photo-me.eu/repository/megroup-reworked-modularization-software-dev-repo/" 
choco source add -n kisproductsdevrework -s="https://nexus.dev.photo-me.eu/repository/megroup-reworked-modularization-product-dev-repo/" 
choco source add -n kisconfigurationdevrework -s="https://nexus.dev.photo-me.eu/repository/megroup-reworked-modularization-configuration-dev-repo/" 
#choco source add -n hanoi_kis_software_dev -s="https://nexus.dev.photo-me.eu/repository/megroup-reworked-modularization-software-dev-repo/" 
#choco source add -n hanoi_kis_products_dev -s="https://nexus.dev.photo-me.eu/repository/megroup-reworked-modularization-product-dev-repo/" 
#choco source add -n hanoi_kis_configuration_dev -s="https://nexus.dev.photo-me.eu/repository/megroup-reworked-modularization-configuration-dev-repo/" 

# Setup configurations

choco uninstall operator-photomaton --force -y
choco install operator-photomaton --version=3.0.0 --force -y

choco uninstall generic-configuration --force -y
choco install generic-configuration --version=3.4.0-alpha-a --force -y

choco uninstall install-redist --force -y
choco install install-redist --version=3.1.0 --force -y

choco uninstall machine-configuration --force -y
choco install machine-configuration --version=3.0.0 --force -y

#NOT YET SETUP THIS
choco uninstall workflow-photomaton-default --force -y
choco install workflow-photomaton-default --version=3.4.0-alpha-a --force -y


# Setup products

choco uninstall product-france-photo-official-idpassport-x6 --force -y
choco install product-france-photo-official-idpassport-x6 --version=3.1.8 --force -y

choco uninstall product-common-photo-official-visa-default --force -y
choco install product-common-photo-official-visa-default --version=3.1.0 --force -y

choco uninstall product-france-photo-official-drivinglicense-x4 --force -y
choco install product-france-photo-official-drivinglicense-x4 --version=3.1.8 --force -y

choco uninstall product-france-photo-official-residencepermit-x4 --force -y
choco install product-france-photo-official-residencepermit-x4 --version=3.1.8 --force -y

choco uninstall product-france-photo-fun-vintage-default --force -y
choco install product-france-photo-fun-vintage-default --version=3.1.8 --force -y

choco uninstall product-france-photo-fun-portrait-default --force -y
choco install product-france-photo-fun-portrait-default --version=3.4.0-alpha-a --force -y

choco uninstall product-france-photo-fun-portrait-x4 --force -y
choco install product-france-photo-fun-portrait-x4 --version=3.1.8 --force -y

choco uninstall product-france-photo-fun-portrait-x16 --force -y
choco install product-france-photo-fun-portrait-x16 --version=3.1.8 --force -y



# Setup software

choco uninstall services-launcher --force -y
choco install services-launcher --version=3.1.2 --force -y

choco uninstall service-ui --force -y
choco install service-ui --version=3.4.0-alpha-a --force -y

choco uninstall service-payment --force -y
choco install service-payment --version=3.1.2 --force -y

choco uninstall payment-device-castle --force -y
choco install payment-device-castle --version=3.1.0 --force -y

choco uninstall payment-device-bill --force -y
choco install payment-device-bill --version=3.0.0 --force -y

choco uninstall payment-device-coin --force -y
choco install payment-device-coin --version=3.0.0 --force -y

choco uninstall service-photo-printer --force -y
choco install service-photo-printer --version=3.1.0 --force -y

choco uninstall service-ioboard --force -y
choco install service-ioboard --version=3.1.1 --force -y

choco uninstall service-imagechain --force -y
choco install service-imagechain --version=3.1.5 --force -y

choco uninstall service-remote-transfer --force -y
choco install service-remote-transfer --version=3.1.0 --force -y

choco uninstall service-external-display --force -y
choco install service-external-display --version=3.1.0 --force -y

choco uninstall service-telemetry --force -y
choco install service-telemetry --version=3.1.0 --force -y

choco uninstall kis-face-recognition-binaries --force -y
choco install kis-face-recognition-binaries --version=3.0.0 --force -y

choco uninstall session-viewer --force -y
choco install session-viewer --version=3.0.0 --force -y

choco uninstall camera-calibration-tool --force -y
choco install camera-calibration-tool --version=3.0.0 --force -y

choco uninstall ihmtech-launcher --force -y
choco install ihmtech-launcher --version=3.1.1 --force -y

# Phase install

choco uninstall universal-setup --force -y
choco install universal-setup --version=3.1.1 --force -y

choco uninstall booth-setup --force -y
choco install booth-setup --version=3.1.1 --force -y

choco uninstall remote-updater --force -y
choco install remote-updater --version=3.1.9 --force -y








