function DisplayTitle {
    param (
        $Message
    )
    Write-Host $Message -ForegroundColor White -BackgroundColor DarkGreen
}

function DisplayMessage {
    param (
        $Message
    )
    Write-Host $Message -ForegroundColor Green
}

function DisplayError {
    param (
        $ErrorMessage
    )
    Write-Host $ErrorMessage -ForegroundColor White -BackgroundColor Red
}


if ($args.Count -ne 1)
{
	DisplayError "Missing argument(s)"
	DisplayError "Usage InstallVersion <Version>"
	exit 1
}

# Set choco sources
DisplayTitle "Setting Chocolatey sources"
choco source add -n kisSoftwareDevRework -s="https://nexus.dev.photo-me.eu/repository/megroup-reworked-modularization-software-dev-repo/" 
DisplayMessage "Setting kisSoftwareDevRework sources"
choco source add -n kisProductsDevRework -s="https://nexus.dev.photo-me.eu/repository/megroup-reworked-modularization-product-dev-repo/" 
DisplayMessage "Setting kisProductsDevRework sources"
choco source add -n kisConfigurationDevRework -s="https://nexus.dev.photo-me.eu/repository/megroup-reworked-modularization-configuration-dev-repo/" 
DisplayMessage "Setting kisConfigurationDevRework sources"

$Version=$args[0]
DisplayTitle "Installing version $Version"

# Setup configurations
choco uninstall generic-configuration --force -y
choco install generic-configuration --version=$Version --force -y

choco uninstall machine-configuration --force -y
choco install machine-configuration --version=$Version --force -y

choco uninstall operator-photomaton --force -y
choco install operator-photomaton --version=$Version --force -y

choco uninstall workflow-photomaton-default --force -y
choco install workflow-photomaton-default --version=$Version --force -y

# Setup products
choco uninstall product-common-photo-official-visa-default --force -y
choco install product-common-photo-official-visa-default --version=$Version --force -y

choco uninstall product-france-photo-fun-portrait-default --force -y
choco install product-france-photo-fun-portrait-default --version=$Version --force -y

choco uninstall product-france-photo-fun-portrait-x16 --force -y
choco install product-france-photo-fun-portrait-x16 --version=$Version --force -y

choco uninstall product-france-photo-fun-portrait-x4 --force -y
choco install product-france-photo-fun-portrait-x4 --version=$Version --force -y

choco uninstall product-france-photo-fun-vintage-default --force -y
choco install product-france-photo-fun-vintage-default --version=$Version --force -y

choco uninstall product-france-photo-official-drivinglicense-x4 --force -y
choco install product-france-photo-official-drivinglicense-x4 --version=$Version --force -y

choco uninstall product-france-photo-official-idpassport-x6 --force -y
choco install product-france-photo-official-idpassport-x6 --version=$Version --force -y

choco uninstall product-france-photo-official-residencepermit-x4 --force -y
choco install product-france-photo-official-residencepermit-x4 --version=$Version --force -y

# Setup software

choco uninstall booth-setup --force -y
choco install booth-setup --version=$Version --force -y

choco uninstall camera-calibration-tool --force -y
choco install camera-calibration-tool --version=$Version --force -y

choco uninstall kis-face-recognition-binaries --force -y
choco install kis-face-recognition-binaries --version=$Version --force -y

choco uninstall payment-device-bill --force -y
choco install payment-device-bill --version=$Version --force -y

choco uninstall payment-device-castle --force -y
choco install payment-device-castle --version=$Version --force -y

choco uninstall payment-device-coin --force -y
choco install payment-device-coin --version=$Version --force -y

choco uninstall remote-updater --force -y
choco install remote-updater --version=$Version --force -y

choco uninstall service-external-display --force -y
choco install service-external-display --version=$Version --force -y

choco uninstall service-imagechain --force -y
choco install service-imagechain --version=$Version --force -y

choco uninstall service-ioboard --force -y
choco install service-ioboard --version=$Version --force -y

choco uninstall service-payment --force -y
choco install service-payment --version=$Version --force -y

choco uninstall service-photo-printer --force -y
choco install service-photo-printer --version=$Version --force -y

choco uninstall service-remote-transfer --force -y
choco install service-remote-transfer --version=$Version --force -y

choco uninstall service-telemetry --force -y
choco install service-telemetry --version=$Version --force -y

choco uninstall service-ui --force -y
choco install service-ui --version=$Version --force -y

choco uninstall services-launcher --force -y
choco install services-launcher --version=$Version --force -y

choco uninstall session-viewer --force -y
choco install session-viewer --version=$Version --force -y

choco uninstall universal-setup --force -y
choco install universal-setup --version=$Version --force -y

choco uninstall ihmtech-launcher --force -y
choco install ihmtech-launcher --version=$Version --force -y