function DisplayTitle {
    param (
        $Message
    )
    Write-Host $Message -ForegroundColor White -BackgroundColor DarkGreen
}

function DisplayMessage {
    param (
        $Message
    )
    Write-Host $Message -ForegroundColor Green
}

function DisplayError {
    param (
        $ErrorMessage
    )
    Write-Host $ErrorMessage -ForegroundColor White -BackgroundColor Red
}

# Searchs <version> in provided file and return it
function ReadPackageVersionInFile($filename) {
	try {
		$fileContent=[string](Get-Content "$filename")
	
		$regex="<version>(.*?)<\/version>"
		$fileContent -match $regex >$null
		$foundPackageVersion=$matches[1]
	
		DisplayMessage "Found PackageVersion: $foundPackageVersion in '$filename'"
	
		return $foundPackageVersion
	}
	catch {
		DisplayMessage "No PackageVersion found in '$filename' \n"
		return $null
	}	
}

# Searchs <PackageId> in provided file and return it
function ReadPackageIdInFile($filename) {
	try {
		$fileContent=[string](Get-Content "$filename")
	
		$regex="<PackageId>(.*?)<\/PackageId>"
		$fileContent -match $regex >$null
		$foundPackageId=$matches[1]
	
		DisplayMessage "Found PackageId: $foundPackageId in '$filename'"
	
		return $foundPackageId
	}
	catch {
		DisplayMessage "No PackageId found in '$filename' \n"
		return $null
	}	
}

if ($args.Count -ne 2)
{
	DisplayError "Missing argument(s)" -ForegroundColor White -BackgroundColor Red
	DisplayError "Usage choco-packAll <repo root> <package config (Release or Debug)>" -ForegroundColor White -BackgroundColor Red
	exit 1
}

$repoRoot=$args[0]	
$packageConfig=$args[1]
if ($packageConfig -ne "Release" -And $packageConfig -ne "Debug") 
{
	DisplayError "Invalid $packageConfig package config. Supported values are Release or Debug"
	exit 1
}
	
$NexusBaseUrl="https://nexus.dev.photo-me.eu"
$NexusRepoName="megroup-reworked-modularization-product-dev-repo"
$NexusApiKey="bd724a98-56e4-398f-b439-b221ac552188"

$PublishRoot=Join-Path -Path $args[0] -ChildPath "\package\KIS_ROOT\app\setup"
$RepoPath=git rev-parse --show-toplevel
$RepoName=Split-Path -Path $RepoPath -Leaf
$BranchName=git branch --show-current

DisplayTitle "Choco pack $RepoName packages from repo $repoRoot (branch: $BranchName and config: $packageConfig)" -ForegroundColor DarkBlue -BackgroundColor White

$PACKABLE_PROJECTS=$(ls -r *.csproj | Select-String "<IsPackable>true</IsPackable>" | select path).path

DisplayMessage "PACKABLE_PROJECTS: $PACKABLE_PROJECTS"


foreach ($p in $PACKABLE_PROJECTS)
{		
	$packageId = ReadPackageIdInFile $p
	if ($packageId)
	{
		$packageVersion = ReadPackageVersionInFile "$repoRoot\package\${packageId}.nuspec"
		if ($packageVersion -And $BranchName)
		{
			if (($BranchName.Contains("feature")) -And (-not ($packageVersion.Contains("alpha"))))
			{
				DisplayError "Do not pack STABLE package from $BranchName branch"
			}
			elseif ((($BranchName.Contains("develop")) -Or ($BranchName.Contains("master")) -Or ($BranchName.Contains("release"))) -And ($packageVersion.Contains("alpha")))
			{
				DisplayError "Do not pack UNSTABLE package from $BranchName branch"
			}
			else
			{
				.\package\choco-pack.ps1 $repoRoot $BranchName $packageID $packageConfig $NexusBaseUrl $NexusRepoName $NexusApiKey
			}
		}
	}
}