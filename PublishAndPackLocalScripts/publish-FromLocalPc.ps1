$ErrorActionPreference = "Stop"

function DisplayTitle {
    param (
        $Message
    )
    Write-Host $Message -ForegroundColor Yellow
}

function DisplayMessage {
    param (
        $Message
    )
    Write-Host $Message -ForegroundColor Green
}

function DisplayError {
    param (
        $ErrorMessage
    )
    Write-Host $ErrorMessage -ForegroundColor Red
}

# Searchs <PackageId> in provided file and return it
function ReadPackageIdInFile($filename) {
	try {
		$fileContent=[string](Get-Content "$filename")
	
		$regex="<PackageId>(.*?)<\/PackageId>"
		$fileContent -match $regex >$null
		$foundPackageId=$matches[1]
	
		DisplayMessage "Found PackageId: $foundPackageId in '$filename'"
	
		return $foundPackageId
	}
	catch {
		DisplayMessage "No PackageId found in '$filename'"
		return $null
	}	
}

# Searchs <version> in provided file and return it
function ReadVersionInFile($filename) {
	$fileContent=[string](Get-Content -Path $filename)
	$regex='<version>(.*?)<\/version>'
	$fileContent -match "$regex" >$null
	$foundVersion=$Matches[1]
	return $foundVersion
}

try
{
	if ($args.Count -ne 3)
	{
		DisplayError "Missing argument(s)"
		DisplayError "Usage publish <repo root> <module to publish fullpath csproj file> <publish fullpath root directory>"
		exit 1
	}
	
	$repoRoot=$args[0]
	$csprojFullPath=$args[1]
	$moduleFullpath=Split-Path -Path $csprojFullPath -Parent
	$moduleName=ReadPackageIdInFile $csprojFullPath
	
	$publishDir=Join-Path -Path $args[2] -ChildPath "\$moduleName\KIS_ROOT\products\bin"

	$callingDirectory=$pwd
	
	$branchName=git branch --show-current

	DisplayTitle "Current branch is $branchName"
	DisplayTitle "Publising module: $moduleName from csproj: $csprojFullPath in publishDir: $publishDir"

	# Read version from nuspec
	# =======================================================================================
	DisplayTitle "Reading package version from $moduleName.nuspec"
	cd .\package
	$versionFound = ReadVersionInFile ".\$moduleName.nuspec"
	
	$isUnstable=$false
	$publishConfiguration="Release"
	if ($versionFound -match "-alpha") {
		$isUnstable=$true
		$publishConfiguration="Debug"
	}

	DisplayMessage "versionFound: $versionFound, $(if ($isUnstable -eq $true) {'UNSTABLE'} else {'STABLE'})"

	# If stable but on develop or master or release* then warning
	if ($isUnstable -eq $false -and $branchName -ne "develop" -and $branchName -ne "master" -and !$branchName.Contains("release") ) {
		DisplayError "Avoid packing stable package from other branches than develop or master or release (currently $branchName)"
		exit 0
	}

	# If unstable but on develop or master pr release* then error
	if ($isUnstable -eq $true -and ( $branchName -eq "develop" -or $branchName -eq "master" -or $branchName -contains "release" ) ) {
		DisplayError "Avoid packing unstable package on branches develop or master or release (currently $branchName)"
		exit -1
	}

	# Publish 
	# =======================================================================================
	DisplayTitle "Publishing $moduleName ($publishConfiguration) in $publishDir"

	Remove-Item $publishDir -Recurse -ErrorAction Ignore
	cd "$moduleFullpath"

	dotnet restore --no-cache --force
	dotnet publish -c $publishConfiguration -r win-x64 -o $publishDir --self-contained false

	# Generating ZIP file x changing css 
	# =======================================================================================
	DisplayTitle "Adapting css"
	cd "$repoRoot/package/KIS_ROOT_PRODUCTS/$moduleName/KIS_ROOT/products"
	$cssName = Get-ChildItem -Path "css" -Recurse -File | Select-Object -ExpandProperty FullName

	
	(Get-Content -Path $cssName) |
    ForEach-Object {$_ -Replace '_content', '../bin/wwwroot/_content'} |
        Set-Content -Path $cssName

	DisplayTitle "Generating ZIP file"
	cd "$dir/.."
	
	
	DisplayMessage "publishDir: $publishDir"	
	
	# zip output folder to avoid filename too long error
	Compress-Archive -Path $repoRoot/package/KIS_ROOT_PRODUCTS/$moduleName/KIS_ROOT -DestinationPath $repoRoot/package/KIS_ROOT_PRODUCTS/$moduleName/KIS_ROOT/$moduleName.zip -Update
	
	$ExitStatus = 0
}
catch
{
	Write-Host "An error occurred:"
	Write-Host $_
	$ExitStatus = -1
}
finally
{
    cd "$callingDirectory"
	exit $ExitStatus
}