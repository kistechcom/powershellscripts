function DisplayTitle {
    param (
        $Message
    )
    Write-Host $Message -ForegroundColor White -BackgroundColor DarkGreen
}

function DisplayMessage {
    param (
        $Message
    )
    Write-Host $Message -ForegroundColor Green
}

function DisplayError {
    param (
        $ErrorMessage
    )
    Write-Host $ErrorMessage -ForegroundColor White -BackgroundColor Red
}

if ($args.Count -ne 1)
{
	DisplayError "Missing argument(s)"
	DisplayError "Usage publishAll REPO_ROOT"
	exit 1
}

$REPO_ROOT=$args[0]
$PUBLISH_ROOT=Join-Path -Path $args[0] -ChildPath "\package\KIS_ROOT_PRODUCTS"
$REPO_PATH=git rev-parse --show-toplevel
$REPO_NAME=Split-Path -Path $REPO_PATH -Leaf
$BRANCH_NAME=git branch --show-current

DisplayTitle "Publishing $REPO_NAME packages from repo $REPO_ROOT (branch: $BRANCH_NAME)"

$PACKABLE_PROJECTS=$(ls -r *.csproj | Select-String "<IsPackable>true</IsPackable>" | select path)

foreach ($p in $PACKABLE_PROJECTS)
{
	.\sources\ci\scripts\publish-FromLocalPc.ps1 $REPO_ROOT $p.path $PUBLISH_ROOT
		
	if ($LASTEXITCODE -eq -1)
	{
		DisplayTitle "LASTEXITCODE: $LASTEXITCODE for $p"
		exit -1
	}
}