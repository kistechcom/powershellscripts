function DisplayTitle {
    param (
        $Message
    )
    Write-Host $Message -ForegroundColor White -BackgroundColor DarkGreen
}

function DisplayMessage {
    param (
        $Message
    )
    Write-Host $Message -ForegroundColor Green
}

function DisplayError {
    param (
        $ErrorMessage
    )
    Write-Host $ErrorMessage -ForegroundColor White -BackgroundColor Red
}

# Searchs <version> in provided file and return it
function ReadVersionInFile($filename) {
	$fileContent=[string](Get-Content -Path $filename)
	$regex='<version>(.*?)<\/version>'
	$fileContent -match "$regex" >$null
	$foundVersion=$Matches[1]
	return $foundVersion
}

try
{
	if ($args.Count -ne 7)
	{
		DisplayError "Missing argument(s)"
		DisplayError "Usage choco-pack <repo root> <branch name> <package to pack name (must match nuspec filename)> <package config (Release or Debug)> <Nexus base URL> <Nexus repo name> <Nexus api key>"
		exit 1
	}
	# Force path to reload
	$env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path","User")

	$repoRoot=$args[0]	
	$branchName=$args[1]
	$packageName=$args[2]
	$packageConfig=$args[3]
	if ($packageConfig -ne "Release" -And $packageConfig -ne "Debug") 
	{
		DisplayError "Invalid $packageConfig package config. Supported values are Release or Debug"
		exit 1
	}
	
	$NexusBaseUrl=$args[4]
	$NexusRepoName=$args[5]
	$NexusApiKey=$args[6]
	$NexusHostedUrl="$NexusBaseUrl/repository/$NexusRepoName/"

	cd "${repoRoot}"

	DisplayTitle "Enter choco-pack repoRoot: $repoRoot, packageName: $packageName, packageConfig: $packageConfig"

	$packageVersion = ReadVersionInFile "package\$packageName.nuspec"

	DisplayTitle "Generating Chocolatey package for ${packageName} V $packageVersion (package config: $packageConfig)..."
	
	$cmd="choco pack ""$repoRoot\package\${packageName}.nuspec"" --outputdirectory ""$repoRoot"" configuration=$packageConfig -y --allow-unofficial"
	DisplayMessage $cmd	
	Invoke-Expression "$cmd"
	DisplayMessage "PACK LASTEXITCODE: $LASTEXITCODE"
	 if ($LASTEXITCODE -ne 0) {
		DisplayError "Choco pack failed"
		exit -1
	}

	$packagePath="$repoRoot\${packageName}.$packageVersion.nupkg"
	DisplayTitle "Pushing Chocolatey package $packagePath to $NexusHostedUrl..."
	$cmd="choco push $packagePath --source $NexusHostedUrl --allow-unofficial --api-key ${NexusApiKey} --force --confirm"
	DisplayMessage $cmd	
	Invoke-Expression "$cmd"
	DisplayMessage "PUSH LASTEXITCODE: $LASTEXITCODE"
	if ($LASTEXITCODE -ne 0) {
		DisplayError "Choco push failed"
		exit -1
	}

	# call product info publisher
	cd $repoRoot
	DisplayTitle ".\publisher\product-informations-publisher.exe $repoRoot $branchName"
	.\publisher\product-informations-publisher.exe $repoRoot $branchName
}
finally
{
    cd "$callingDirectory"
}