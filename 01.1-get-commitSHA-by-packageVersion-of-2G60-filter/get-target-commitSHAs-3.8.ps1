function DisplayTitle {
    param (
        $Message
    )
    Write-Host $Message -ForegroundColor Yellow
}

function DisplayMessage {
    param (
        $Message
    )
    Write-Host $Message -ForegroundColor Green
}

function DisplayError {
    param (
        $ErrorMessage
    )
    Write-Host $ErrorMessage -ForegroundColor Red
}

# if ($args.Count -ne 2)
# {
# 	DisplayError "Missing argument(s)"
# 	DisplayError "Usage: demo {path to paths.txt} {path to commitsSHA.txt}. eg. .\checkout-all-repo.ps1 ./paths.txt ./commitsSHA.txt"
# 	exit 1
# }

if ($args.Count -ne 1)
{
	DisplayError "Missing argument(s)"
	DisplayError "Usage: checkout-all-repo {path to target-commits.txt}. eg. .\get-target-commitSHAs.ps1 target-versions.txt"
	# > .\get-target-commitSHAs.ps1 $pwd/target-versions.txt
	exit 1
}

DisplayMessage "Current working directory: "
Write-Output $PSScriptRoot
$res = 0
$commits_list_filepath = Join-Path $PSScriptRoot $args[0]
DisplayMessage $commits_list_filepath

foreach($line in [System.IO.File]::ReadLines($commits_list_filepath))
{
    Write-Output $line
    $arr = $line -split "\s+"
    DisplayMessage "Expect dir:$($arr[0])"
    Set-Location -Path $arr[0]
    DisplayMessage "Expect Version:$($arr[1])"
    DisplayMessage "Expect Nuspec:$($arr[2])" # >> "D:\Working\install\nextgen\01.1-get-commitSHA-by-packageVersion-of-2G60-filter\log.txt"
    DisplayMessage "Current dir:$(Get-Location)"

    DisplayMessage "Get CommitSHA of version: $($arr[1])"
    $ver = "<version>" + $arr[1] + "</version>"
    DisplayMessage "Search version text: $ver"
    #git grep -n $ver -- $arr[2] >> "D:\Working\install\nextgen\01.1-get-commitSHA-by-packageVersion-of-2G60-filter\log.txt"
    $x = git log -S $ver --pretty=format:"%H" --branches="release/*" | Select -Last 1  #>> "D:\Working\install\nextgen\01.1-get-commitSHA-by-packageVersion-of-2G60-filter\log.txt"
    DisplayMessage "x: $x"
    #"$($arr[2]): $x" | Out-File -FilePath "D:\Working\install\nextgen\01.1-get-commitSHA-by-packageVersion-of-2G60-filter\log.txt" -Append
    #WARNING WARNING WARING ===== HARD CODE FOR 3.8 =======
    $z = $x + "..release/3.8"
    DisplayError "z: $z"
    $y = git log --ancestry-path $z --merges --pretty=format:"%H" | Select -Last 1
    DisplayMessage "y: $y"
    if ([string]::IsNullOrEmpty($y))
    {
        DisplayMessage "y: IsNullOrEmpty"
        $y = $x
    } else {
        DisplayTitle "FOUND Y"
    }
    "$($arr[2]): $y" | Out-File -FilePath "D:\Working\install\nextgen\01.1-get-commitSHA-by-packageVersion-of-2G60-filter\log.txt" -Append

    #DisplayTitle "commitSHA: $commitSHA"
    #git log --reverse --ancestry-path a0f679f87ceb3d136ed62e5e568607ca74c5fc6f^..release/3.9 --merges

    # verify
    #$comm = git rev-parse --verify HEAD
    #DisplayMessage "Current Commit:$comm."
    #if ($comm.Trim() -ne $arr[1].Trim()) {
    #    DisplayError "Commit SHA is not the same for $arr[0]"
    #    $res = 1
    #}
}

if ($res -eq 1)
{
    DisplayError "Not All Commits Matched!"
} else {
    DisplayTitle "Successful. All Commit Are Matched!"
}

Set-Location -Path $PSScriptRoot